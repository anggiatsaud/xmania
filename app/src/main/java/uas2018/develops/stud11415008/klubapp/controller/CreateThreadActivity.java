package uas2018.develops.stud11415008.klubapp.controller;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;
import uas2018.develops.stud11415008.klubapp.R;

public class CreateThreadActivity extends AppCompatActivity {

    private static final String TAG = "CreateThreadActivity";

    private ImageView iv_thread;
    private EditText et_desc_thread;
    private Button btn_add_thread;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;

    private Uri uriImage;

    private String downloadUrl;

    //Firebase
    private StorageReference storageReference;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private FirebaseFirestore firebaseFirestore;

    private String key_thread, current_user_id, desc, randomName, dateTime;

    private long countPosts = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_create_thread);

        toolbar = findViewById(R.id.create_post_thread_page_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Create Thread");

        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference("Threads");
        firebaseFirestore = FirebaseFirestore.getInstance();

        current_user_id = firebaseAuth.getCurrentUser().getUid();

        iv_thread = findViewById(R.id.image_thread);
        et_desc_thread = findViewById(R.id.et_desc);
        btn_add_thread = findViewById(R.id.btn_add_thread);
        progressDialog = new ProgressDialog(this);

        iv_thread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestImage();
            }
        });

        btn_add_thread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateRequest();
            }
        });

    }

    private void validateRequest(){
        desc = et_desc_thread.getText().toString();

        if (uriImage == null){
            Toast.makeText(this, "Please Insert your Images", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(desc)){
            Toast.makeText(this, "Please Insert All Field", Toast.LENGTH_SHORT).show();
        }else{

            progressDialog.setTitle("Created Thread");
            progressDialog.setMessage("Please waiting");
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(true);

            saveImageToStorage();
        }

    }

    private void saveImageToStorage(){

        randomName = UUID.randomUUID().toString();

        StorageReference filePath = storageReference.child("Thread_Images").child("Thread-" + randomName + ".jpg");

        filePath.putFile(uriImage).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    downloadUrl = task.getResult().getDownloadUrl().toString();
                    Toast.makeText(CreateThreadActivity.this, "image uploaded", Toast.LENGTH_SHORT).show();

                    saveInfoToStorage();

                }else {
                    progressDialog.dismiss();
                    String message = task.getException().getMessage();
                    Toast.makeText(CreateThreadActivity.this, "Error " + message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveInfoToStorage() {

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    countPosts = dataSnapshot.getChildrenCount();
                }else {
                    countPosts = 0;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat currentDate = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        dateTime = currentDate.format(calendar.getTime());
        key_thread = "Thread-"+ current_user_id + "-" + randomName;

        HashMap threadMap = new HashMap();
        threadMap.put("key_thread", key_thread);
        threadMap.put("image_url", downloadUrl);
        threadMap.put("user_key", current_user_id);
        threadMap.put("desc", desc);
        threadMap.put("timestamp", dateTime);
        threadMap.put("counter", countPosts);

        databaseReference.child(key_thread).updateChildren(threadMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    progressDialog.dismiss();
                    SendUserToDashboardActivity();
                    Toast.makeText(CreateThreadActivity.this, "Thread is updated successfully.", Toast.LENGTH_SHORT).show();
                }else{
                    progressDialog.dismiss();
                    Toast.makeText(CreateThreadActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            SendUserToDashboardActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    private void SendUserToDashboardActivity() {
        Intent dashboardIntent = new Intent(CreateThreadActivity.this, DashboardActivity.class);
        startActivity(dashboardIntent);
        finish();
    }

    private void setRequestImage(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriImage = result.getUri();
                iv_thread.setImageURI(uriImage);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
