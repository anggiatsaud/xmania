package uas2018.develops.stud11415008.klubapp.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.MessageAdapter;
import uas2018.develops.stud11415008.klubapp.adapter.ThreadAdapter;
import uas2018.develops.stud11415008.klubapp.model.Message;
import uas2018.develops.stud11415008.klubapp.tag.TagMessage;

public class ChatActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private ImageButton sendMessageButton;
    private EditText textMessageInput;
    private RecyclerView messageList;

    private TextView receiverName;
    private CircleImageView receiverProfileImage;

    private TagMessage tagMessage;

    private DatabaseReference friendRef, rootRef, user_message_key, messageRef;
    private FirebaseAuth auth;

    private String receiverUserId, messageSenderId;

    private List<Message> messages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        auth = FirebaseAuth.getInstance();
        messageSenderId = auth.getCurrentUser().getUid();

        tagMessage = new TagMessage();
        receiverUserId = getIntent().getExtras().get(tagMessage.MESSAGE_KEY_USER).toString();
        rootRef = FirebaseDatabase.getInstance().getReference();
        friendRef = FirebaseDatabase.getInstance().getReference().child("Users").child(receiverUserId);

        messageRef = FirebaseDatabase.getInstance().getReference().child("Messages").child(messageSenderId).child(receiverUserId);
        IntializeFields();

        loadActionBar();

        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
    }

    private void loadActionBar() {
        friendRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String filename = dataSnapshot.child("full_name").getValue().toString();
                    String image = dataSnapshot.child("image_url").getValue().toString();

                    receiverName.setText(filename);
                    Glide.with(ChatActivity.this)
                            .load(image)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(receiverProfileImage);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendMessage() {
        String textMessage = textMessageInput.getText().toString();

        if (TextUtils.isEmpty(textMessage)){
            Toast.makeText(this, "Please type a message first...", Toast.LENGTH_SHORT).show();
        }else{
            String message_sender_ref = "Messages/" + messageSenderId + "/" + receiverUserId;
            String message_receiver_ref = "Messages/" + receiverUserId + "/" + messageSenderId;

            user_message_key = rootRef.child("Messages").child(messageSenderId).child(receiverUserId).push();

            String message_push_id = user_message_key.getKey();

            Calendar calendarDate = Calendar.getInstance();
            @SuppressLint("SimpleDateFormat") SimpleDateFormat currentDate = new SimpleDateFormat("dd MMMM yyyy");
            String dateTime = currentDate.format(calendarDate.getTime());

            @SuppressLint("SimpleDateFormat") SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm aa");
            String timeStamp = currentTime.format(calendarDate.getTime());

            Map messageTextBody = new HashMap();
            messageTextBody.put("message", textMessage);
            messageTextBody.put("date", dateTime);
            messageTextBody.put("time", timeStamp);
            messageTextBody.put("type", "text");
            messageTextBody.put("from", messageSenderId);

            Map messageTextDetails = new HashMap();
            messageTextDetails.put(message_sender_ref + "/" + message_push_id, messageTextBody);
            messageTextDetails.put(message_receiver_ref + "/" + message_push_id, messageTextBody);

            rootRef.updateChildren(messageTextDetails).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()){
                        Toast.makeText(ChatActivity.this, "Message Sent Successfully...", Toast.LENGTH_SHORT).show();
                        textMessageInput.setText("");
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        messageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    messages.clear();

                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        Message message = ds.getValue(Message.class);
                        messages.add(message);
                    }

                    messageList.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
                    MessageAdapter messageAdapter = new MessageAdapter(ChatActivity.this);
                    messageAdapter.setMessages(messages);
                    messageList.smoothScrollToPosition(messageAdapter.getItemCount()-1);
                    messageList.setAdapter(messageAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void IntializeFields() {
        toolbar = findViewById(R.id.toolbar_chat_activity);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater layoutInflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = layoutInflater.inflate(R.layout.chat_custom_bar,null);
        actionBar.setCustomView(action_bar_view);

        receiverName = action_bar_view.findViewById(R.id.custom_profile_name);
        receiverProfileImage = action_bar_view.findViewById(R.id.custom_profile_image);

        sendMessageButton = findViewById(R.id.send_message_button);
        textMessageInput = findViewById(R.id.input_message);

        messageList = findViewById(R.id.messages_list_user);
        messageList.setHasFixedSize(true);
    }


}
