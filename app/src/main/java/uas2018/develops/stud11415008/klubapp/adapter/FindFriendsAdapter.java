package uas2018.develops.stud11415008.klubapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.controller.PersonProfileActivity;
import uas2018.develops.stud11415008.klubapp.model.FindFriend;
import uas2018.develops.stud11415008.klubapp.tag.TagMessage;

public class FindFriendsAdapter extends RecyclerView.Adapter<FindFriendsAdapter.ViewHolder> {

    private List<FindFriend> findFriends;

    private Context context;

    public FindFriendsAdapter(Context context) {
        this.context = context;
    }

    public void setFindFriends(List<FindFriend> findFriends) {
        this.findFriends = findFriends;
    }

    private TagMessage tagMessage;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        tagMessage = new TagMessage();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tv_name.setText(findFriends.get(position).getFull_name());
        holder.tv_status.setText(findFriends.get(position).getStatus());

        Glide.with(context)
                .load(findFriends.get(position).getImage_url())
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.civ_image);

        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_key = findFriends.get(position).getUser_key();
                Intent peronIntent = new Intent(context, PersonProfileActivity.class);
                peronIntent.putExtra(tagMessage.MESSAGE_KEY_USER, user_key);
                context.startActivity(peronIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return findFriends.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_status;
        private CircleImageView civ_image;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.user_profile_full_name);
            tv_status = itemView.findViewById(R.id.user_profile_status);
            civ_image = itemView.findViewById(R.id.user_profile_image);

        }
    }
}
