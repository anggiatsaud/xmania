package uas2018.develops.stud11415008.klubapp.controller;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.tag.TagMessage;

public class PersonProfileActivity extends AppCompatActivity {

    private TextView myFullName, myUsername, myStatus, myGender, myDOB;
    private CircleImageView myImageView;
    private ProgressDialog progressDialog;

    private Button send_friend_request, decline_friend_request;

    private DatabaseReference personRef, friendReqRef, friendRef;
    private FirebaseAuth auth;

    private String receiverUserId, senderUserId, CURRENT_STATE;
    private TagMessage tagMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_profile);

        auth = FirebaseAuth.getInstance();
        senderUserId = auth.getCurrentUser().getUid();

        tagMessage = new TagMessage();
        receiverUserId = getIntent().getExtras().get(tagMessage.MESSAGE_KEY_USER).toString();
        personRef = FirebaseDatabase.getInstance().getReference().child("Users").child(receiverUserId);
        friendReqRef = FirebaseDatabase.getInstance().getReference().child("FriendRequests");
        friendRef = FirebaseDatabase.getInstance().getReference().child("Friends");
        initializeFields();

        myFullName = findViewById(R.id.person_profile_full_name);
        myUsername = findViewById(R.id.person_profile_username);
        myStatus = findViewById(R.id.person_profile_status);
        myGender = findViewById(R.id.person_profile_gender);
        myDOB = findViewById(R.id.person_profile_dob);
        myImageView = findViewById(R.id.person_profile_image);

        send_friend_request = findViewById(R.id.person_send_friend_request_btn);
        decline_friend_request = findViewById(R.id.person_decline_friend_request_btn);

        progressDialog = new ProgressDialog(this);

        decline_friend_request.setVisibility(View.INVISIBLE);
        decline_friend_request.setEnabled(false);

        if (!senderUserId.equals(receiverUserId)){
            send_friend_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    send_friend_request.setEnabled(false);

                    if (CURRENT_STATE.equals("not_friends")){
                        sendFriendRequest();
                    }
                    if (CURRENT_STATE.equals("request_sent")){
                        cancelFriendRequest();
                    }
                    if (CURRENT_STATE.equals("request_received")){
                        acceptFriendRequest();
                    }
                    if (CURRENT_STATE.equals("friends")){
                        unfriend();
                    }
                }
            });
        }else {
            decline_friend_request.setVisibility(View.INVISIBLE);
            send_friend_request.setVisibility(View.INVISIBLE);
        }

    }

    private void unfriend() {
        friendRef.child(senderUserId).child(receiverUserId)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            friendRef.child(receiverUserId).child(senderUserId)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                send_friend_request.setEnabled(true);
                                                CURRENT_STATE = "not_friends";
                                                send_friend_request.setText("Send Friend Request");

                                                decline_friend_request.setVisibility(View.INVISIBLE);
                                                decline_friend_request.setEnabled(true);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void acceptFriendRequest() {
        Calendar calendarDate = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat currentDate = new SimpleDateFormat("dd MMMM yyyy");
        final String dateTime = currentDate.format(calendarDate.getTime());

        HashMap friendMap = new HashMap();
        friendMap.put("date", dateTime);
        friendMap.put("key_user", receiverUserId);

        friendRef.child(senderUserId).child(receiverUserId).updateChildren(friendMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            HashMap friendMap = new HashMap();
                            friendMap.put("date", dateTime);
                            friendMap.put("key_user", senderUserId);
                            friendRef.child(receiverUserId).child(senderUserId).updateChildren(friendMap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                friendReqRef.child(senderUserId).child(receiverUserId)
                                                        .removeValue()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()){
                                                                    friendReqRef.child(receiverUserId).child(senderUserId)
                                                                            .removeValue()
                                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                                    if (task.isSuccessful()){
                                                                                        send_friend_request.setEnabled(true);
                                                                                        CURRENT_STATE = "friends";
                                                                                        send_friend_request.setText("Unfriend This Person");

                                                                                        decline_friend_request.setVisibility(View.INVISIBLE);
                                                                                        decline_friend_request.setEnabled(true);
                                                                                    }
                                                                                }
                                                                            });
                                                                }
                                                            }
                                                        });
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void cancelFriendRequest() {
        friendReqRef.child(senderUserId).child(receiverUserId)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            friendReqRef.child(receiverUserId).child(senderUserId)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                send_friend_request.setEnabled(true);
                                                CURRENT_STATE = "not_friends";
                                                send_friend_request.setText("Send Friend Request");

                                                decline_friend_request.setVisibility(View.INVISIBLE);
                                                decline_friend_request.setEnabled(true);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void sendFriendRequest() {
        friendReqRef.child(senderUserId).child(receiverUserId)
                .child("request_type").setValue("sent")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            friendReqRef.child(receiverUserId).child(senderUserId)
                                    .child("request_type").setValue("received")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                send_friend_request.setEnabled(true);
                                                CURRENT_STATE = "request_sent";
                                                send_friend_request.setText("Cancel Friend Request");

                                                decline_friend_request.setVisibility(View.INVISIBLE);
                                                decline_friend_request.setEnabled(true);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void initializeFields(){
        CURRENT_STATE = "not_friends";
    }

    @Override
    protected void onStart() {
        super.onStart();

        progressDialog.setTitle("Showing User Information");
        progressDialog.setMessage("Please wait, while we are showing person information....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        personRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String refUserImage = dataSnapshot.child("image_url").getValue().toString();
                    String refUserStatus = dataSnapshot.child("status").getValue().toString();
                    String refUserName = dataSnapshot.child("name").getValue().toString();
                    String refUserFullName = dataSnapshot.child("full_name").getValue().toString();
                    String refUserDOB = dataSnapshot.child("dob").getValue().toString();
                    String refUserGender = dataSnapshot.child("gender").getValue().toString();

                    Glide.with(PersonProfileActivity.this)
                            .load(refUserImage)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(myImageView);

                    myStatus.setText(refUserStatus);
                    myUsername.setText("@"+refUserName);
                    myFullName.setText(refUserFullName);
                    myDOB.setText("DOB: "+refUserDOB);
                    myGender.setText("Gender: "+refUserGender);

                    maintenanceOfButton();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }

    private void maintenanceOfButton() {
        friendReqRef.child(senderUserId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(receiverUserId)){

                            String request_type = dataSnapshot.child(receiverUserId).child("request_type").getValue().toString();

                                if (request_type.equals("sent")){

                                    CURRENT_STATE = "request_sent";
                                    send_friend_request.setText("Cancel Friend Request");

                                    decline_friend_request.setVisibility(View.INVISIBLE);
                                    decline_friend_request.setEnabled(true);

                                }

                                else if (request_type.equals("received")){
                                    CURRENT_STATE = "request_received";
                                    send_friend_request.setText("Accept Friend Request");

                                    decline_friend_request.setVisibility(View.VISIBLE);
                                    decline_friend_request.setEnabled(true);

                                    decline_friend_request.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            cancelFriendRequest();
                                        }
                                    });
                                }

                        }
                        else
                            {
                            friendRef.child(senderUserId)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {

                                            if (dataSnapshot.hasChild(receiverUserId)){
                                                CURRENT_STATE = "friends";
                                                send_friend_request.setText("Unfriend This Person");

                                                decline_friend_request.setVisibility(View.INVISIBLE);
                                                decline_friend_request.setEnabled(true);
                                            }

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
