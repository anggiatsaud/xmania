package uas2018.develops.stud11415008.klubapp.model;



public class Thread{


    private String key_thread, user_key, image_url, desc, timestamp;

    public Thread() {
    }

    public Thread(String key_thread, String user_key, String image_url, String desc, String timestamp) {
        this.key_thread = key_thread;
        this.user_key = user_key;
        this.image_url = image_url;
        this.desc = desc;
        this.timestamp = timestamp;
    }

    public String getKey_thread() {
        return key_thread;
    }

    public void setKey_thread(String key_thread) {
        this.key_thread = key_thread;
    }

    public String getUser_key() {
        return user_key;
    }

    public void setUser_key(String user_key) {
        this.user_key = user_key;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
