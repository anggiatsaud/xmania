package uas2018.develops.stud11415008.klubapp.model;

public class Comment {
    private String comment_key, user_key, comment, dateTime, timestamp;

    public Comment() {
    }

    public Comment(String comment_key, String user_key, String comment, String dateTime, String timestamp) {
        this.comment_key = comment_key;
        this.user_key = user_key;
        this.comment = comment;
        this.dateTime = dateTime;
        this.timestamp = timestamp;
    }

    public String getComment_key() {
        return comment_key;
    }

    public void setComment_key(String comment_key) {
        this.comment_key = comment_key;
    }

    public String getUser_key() {
        return user_key;
    }

    public void setUser_key(String user_key) {
        this.user_key = user_key;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
