package uas2018.develops.stud11415008.klubapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.model.Message;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private Context context;

    private List<Message> messages;

    private FirebaseAuth auth;
    private DatabaseReference userRef;

    public MessageAdapter(Context context) {
        this.context = context;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_item, parent, false);
        auth = FirebaseAuth.getInstance();
        userRef = FirebaseDatabase.getInstance().getReference().child("Users");
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        String fromUserId = messages.get(position).getFrom();
        String currentUserId = auth.getCurrentUser().getUid();
        String fromMessageTYpe = messages.get(position).getType();

        userRef.child(fromUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String image = dataSnapshot.child("image_url").getValue().toString();
                    Glide.with(context)
                            .load(image)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.image_profile);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (fromMessageTYpe.equals("text")){
            holder.receiver_message.setVisibility(View.INVISIBLE);
            holder.image_profile.setVisibility(View.INVISIBLE);

            if (fromUserId.equals(currentUserId)){
                holder.sender_message.setBackgroundResource(R.drawable.sender_message_text_background);
                holder.sender_message.setTextColor(Color.BLACK);
                holder.sender_message.setGravity(Gravity.START);
                holder.sender_message.setText(messages.get(position).getMessage());
            }else{
                holder.sender_message.setVisibility(View.INVISIBLE);
                holder.receiver_message.setVisibility(View.VISIBLE);
                holder.image_profile.setVisibility(View.VISIBLE);

                holder.receiver_message.setBackgroundResource(R.drawable.receiver_message_text_background);
                holder.receiver_message.setTextColor(Color.WHITE);
                holder.receiver_message.setGravity(Gravity.START);
                holder.receiver_message.setText(messages.get(position).getMessage());
            }
        }

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView image_profile;
        private TextView sender_message;
        private TextView receiver_message;

        public ViewHolder(View itemView) {
            super(itemView);

            image_profile = itemView.findViewById(R.id.message_profile_image);
            sender_message = itemView.findViewById(R.id.sender_message_text);
            receiver_message = itemView.findViewById(R.id.receiver_message_text);
        }
    }
}
