package uas2018.develops.stud11415008.klubapp.controller;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.UserMessageAdapter;
import uas2018.develops.stud11415008.klubapp.model.UserMessage;

public class UserMessageActivity extends AppCompatActivity {

    private RecyclerView list_user;
    private ProgressDialog progressDialog;

    private List<UserMessage> userMessages;

    private FirebaseAuth auth;
    private DatabaseReference userMessage;

    private String currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_message);

        Toolbar toolbar = findViewById(R.id.toolbar_user_message_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Message");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        auth = FirebaseAuth.getInstance();
        currentUserId = auth.getCurrentUser().getUid();

        progressDialog = new ProgressDialog(this);
        list_user = findViewById(R.id.list_item_message);
        userMessages = new ArrayList<>();
        userMessage = FirebaseDatabase.getInstance().getReference().child("Messages").child(currentUserId);

    }

    @Override
    protected void onStart() {
        super.onStart();
        progressDialog.show();
        userMessage.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    userMessages.clear();

                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        String key = ds.getKey();
                        UserMessage userMessage = new UserMessage(key);
                        userMessages.add(userMessage);
                    }

                    list_user.setLayoutManager(new LinearLayoutManager(UserMessageActivity.this));
                    UserMessageAdapter userMessageAdapter = new UserMessageAdapter(UserMessageActivity.this);
                    userMessageAdapter.setUserMessages(userMessages);
                    list_user.setAdapter(userMessageAdapter);

                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
                String message = databaseError.getMessage();
                Log.i("Error: ", message);
            }
        });
    }
}
