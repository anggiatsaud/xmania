package uas2018.develops.stud11415008.klubapp.model;

public class FindFriend {
    public String dob, full_name, gender, image_url, name, status, user_key;

    public FindFriend() {
    }

    public FindFriend(String dob, String full_name, String gender, String image_url, String name, String status, String user_key) {
        this.dob = dob;
        this.full_name = full_name;
        this.gender = gender;
        this.image_url = image_url;
        this.name = name;
        this.status = status;
        this.user_key = user_key;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_key() {
        return user_key;
    }

    public void setUser_key(String user_key) {
        this.user_key = user_key;
    }
}
