package uas2018.develops.stud11415008.klubapp.controller;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.FindFriendsAdapter;
import uas2018.develops.stud11415008.klubapp.model.FindFriend;

public class AccountActivity extends AppCompatActivity {

    private static final String TAG = "AccountActivity";

    private FirebaseAuth auth;
    private DatabaseReference dr_user;
    private StorageReference sr_image;

    String currentUserId;

    private EditText fullName;
    private Button btn_save;
    private CircleImageView profileImage;
    private ProgressDialog progressDialog;

    private Uri uriImage;

    private String downloadUrl, name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        auth = FirebaseAuth.getInstance();
        currentUserId = auth.getCurrentUser().getUid();
        dr_user = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserId);
        sr_image = FirebaseStorage.getInstance().getReference();

        fullName = findViewById(R.id.et_full_name);
        btn_save = findViewById(R.id.btn_profile_setting);
        profileImage = findViewById(R.id.civ_user_image);
        progressDialog = new ProgressDialog(this);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validationRequest();
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestImage();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        dr_user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String fullname = dataSnapshot.child("name").getValue().toString();
                    String image = dataSnapshot.child("image_url").getValue().toString();

                    fullName.setText(fullname);
                    Glide.with(AccountActivity.this)
                            .load(image)
                            .override(200, 200)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(profileImage);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setRequestImage(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    private void validationRequest() {
        name = fullName.getText().toString();
        if (TextUtils.isEmpty(name)){
            Toast.makeText(this, "Please write your name....", Toast.LENGTH_SHORT).show();
        }else if (uriImage == null){
            Toast.makeText(this, "Please Choose your images...", Toast.LENGTH_SHORT).show();
        }
        else{

            progressDialog.setTitle("Saving Information");
            progressDialog.setMessage("Please wait, while we are creating your new account....");
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(true);

            saveImageToStorage();

        }
    }

    private void saveImageToStorage(){
        StorageReference filePath = sr_image.child("profile_images").child(currentUserId+".jpg");
        filePath.putFile(uriImage).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    downloadUrl = task.getResult().getDownloadUrl().toString();
                    saveAccountInformation();

                }else {
                    progressDialog.dismiss();
                    String message = task.getException().getMessage();
                    Toast.makeText(AccountActivity.this, "Error " + message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveAccountInformation(){
        HashMap userMap = new HashMap();
        userMap.put("image_url", downloadUrl);
        userMap.put("name", name);
        userMap.put("status", "none");
        userMap.put("full_name", "none");
        userMap.put("dob", "none");
        userMap.put("gender", "none");
        userMap.put("user_key", currentUserId);

        dr_user.updateChildren(userMap).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()){
                    sendUserToDashboardActivity();
                    Toast.makeText(AccountActivity.this, "Your Account is Created Successfully...", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }else {
                    String message = task.getException().getMessage();
                    Toast.makeText(AccountActivity.this, "Error Occurred: " + message, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void sendUserToDashboardActivity() {
        Intent dashboardIntent = new Intent(AccountActivity.this, DashboardActivity.class);
        dashboardIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(dashboardIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriImage = result.getUri();
                profileImage.setImageURI(uriImage);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}


