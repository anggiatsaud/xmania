package uas2018.develops.stud11415008.klubapp.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;

public class ProfileActivity extends AppCompatActivity {

    private TextView myFullName, myUsername, myStatus, myGender, myDOB;
    private CircleImageView myImageView;
    private ProgressDialog progressDialog;

    private FirebaseAuth auth;
    private DatabaseReference profileRef, threadsRef, friendsRef;
    private Query queryThread;

    private String currentUserId;

    int countFriends, countThreads;

    private Button btn_no_of_posts, btn_no_of_friends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        auth = FirebaseAuth.getInstance();
        currentUserId = auth.getCurrentUser().getUid();

        profileRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserId);
        threadsRef = FirebaseDatabase.getInstance().getReference().child("Threads");
        queryThread = threadsRef.orderByChild("user_key").equalTo(currentUserId);

        friendsRef = FirebaseDatabase.getInstance().getReference().child("Friends").child(currentUserId);

        myFullName = findViewById(R.id.my_profile_full_name);
        myUsername = findViewById(R.id.my_profile_username);
        myStatus = findViewById(R.id.my_profile_status);
        myGender = findViewById(R.id.my_profile_gender);
        myDOB = findViewById(R.id.my_profile_dob);
        myImageView = findViewById(R.id.my_profile_image);
        progressDialog = new ProgressDialog(this);

        btn_no_of_posts = findViewById(R.id.my_post_button);
        btn_no_of_friends = findViewById(R.id.my_friends_button);

        countFriend();
        countThread();

        btn_no_of_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUserToFriendsActivity();
            }
        });

        btn_no_of_posts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUserToMyPostsActivity();
            }
        });
    }

    private void countThread() {
        queryThread.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    countThreads = (int) dataSnapshot.getChildrenCount();
                    btn_no_of_posts.setText(countThreads + " Posts");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void countFriend() {
        friendsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    countFriends = (int) dataSnapshot.getChildrenCount();
                    btn_no_of_friends.setText(countFriends + " Friends");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressDialog.setTitle("Showing Information");
        progressDialog.setMessage("Please wait, while we are showing your information....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        profileRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String refUserImage = dataSnapshot.child("image_url").getValue().toString();
                    String refUserStatus = dataSnapshot.child("status").getValue().toString();
                    String refUserName = dataSnapshot.child("name").getValue().toString();
                    String refUserFullName = dataSnapshot.child("full_name").getValue().toString();
                    String refUserDOB = dataSnapshot.child("dob").getValue().toString();
                    String refUserGender = dataSnapshot.child("gender").getValue().toString();

                    Glide.with(ProfileActivity.this)
                            .load(refUserImage)
                            .override(200, 200)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(myImageView);

                    myStatus.setText(refUserStatus);
                    myUsername.setText("@"+refUserName);
                    myFullName.setText(refUserFullName);
                    myDOB.setText("DOB: "+refUserDOB);
                    myGender.setText("Gender: "+refUserGender);
                }

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }

    private void sendUserToFriendsActivity() {
        Intent friendsIntent = new Intent(ProfileActivity.this, FriendsActivity.class);
        startActivity(friendsIntent);
    }

    private void sendUserToMyPostsActivity() {
        Intent myPostIntent = new Intent(ProfileActivity.this, MyPostsActivity.class);
        startActivity(myPostIntent);
    }

}
