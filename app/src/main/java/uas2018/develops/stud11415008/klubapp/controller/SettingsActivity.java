package uas2018.develops.stud11415008.klubapp.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;

public class SettingsActivity extends AppCompatActivity {

    private CircleImageView civ_user_image;
    private EditText userStatus, userName, userFullName, userdDOB, userGender;
    private Button buttonSettings;
    private ProgressDialog progressDialog;

    private Toolbar toolbar;

    private DatabaseReference settingRef;
    private StorageReference sr_image;
    private FirebaseAuth auth;

    private Uri uriImage;

    private String downloadUrl, currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        auth = FirebaseAuth.getInstance();
        currentUserId = auth.getCurrentUser().getUid();
        settingRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserId);
        sr_image = FirebaseStorage.getInstance().getReference();

        toolbar = findViewById(R.id.toolbar_settings);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Account Settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        civ_user_image = findViewById(R.id.settings_profile_image);
        userStatus = findViewById(R.id.settings_status);
        userName = findViewById(R.id.settings_username);
        userFullName = findViewById(R.id.settings_full_name);
        userdDOB = findViewById(R.id.settings_dob);
        userGender = findViewById(R.id.settings_gender);
        buttonSettings = findViewById(R.id.update_account_settings_button);

        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validationInput();
            }
        });

        civ_user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestImage();
            }
        });
    }

    private void setRequestImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    private void validationInput() {
        String myStatus = userStatus.getText().toString();
        String myName = userName.getText().toString();
        String myFullName = userFullName.getText().toString();
        String myDOB = userdDOB.getText().toString();
        String myGender = userGender.getText().toString();

        if (TextUtils.isEmpty(myStatus)){
            Toast.makeText(this, "Please write your status....", Toast.LENGTH_SHORT).show();
        }else if (uriImage == null){
            Toast.makeText(this, "Please Choose your images...", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(myName)){
            Toast.makeText(this, "Please write your username....", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(myFullName)){
            Toast.makeText(this, "Please write your full name....", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(myDOB)){
            Toast.makeText(this, "Please write your date of birth....", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(myGender)){
            Toast.makeText(this, "Please write your gender....", Toast.LENGTH_SHORT).show();
        }else{
            progressDialog.setTitle("Saving Information");
            progressDialog.setMessage("Please wait, while we are updating your account....");
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(true);
            saveImageToStorage(myStatus, myName, myFullName, myDOB, myGender);
        }
    }

    private void saveImageToStorage(final String myStatus, final String myName, final String myFullName, final String myDOB, final String myGender){
        StorageReference filePath = sr_image.child("profile_images").child(currentUserId+".jpg");
        filePath.putFile(uriImage).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    downloadUrl = task.getResult().getDownloadUrl().toString();
                    updateUserAccount(myStatus, myName, myFullName, myDOB, myGender);

                }else {
                    progressDialog.dismiss();
                    String message = task.getException().getMessage();
                    Toast.makeText(SettingsActivity.this, "Error Occurred: " + message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateUserAccount(String myStatus, String myName, String myFullName, String myDOB, String myGender) {
        HashMap userMap = new HashMap();
            userMap.put("image_url", downloadUrl);
            userMap.put("name", myName);
            userMap.put("status", myStatus);
            userMap.put("full_name", myFullName);
            userMap.put("dob", myDOB);
            userMap.put("gender", myGender);

        settingRef.updateChildren(userMap).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()){
                    progressDialog.dismiss();
                    Toast.makeText(SettingsActivity.this, "Account updated successfully..", Toast.LENGTH_SHORT).show();
                    sendUserToDashboardActivity();
                }else{
                    progressDialog.dismiss();
                    Toast.makeText(SettingsActivity.this, "Error Occurred..", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressDialog.setTitle("Showing User Information");
        progressDialog.setMessage("Please wait, while we are showing your information....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        settingRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String refUserImage = dataSnapshot.child("image_url").getValue().toString();
                    String refUserStatus = dataSnapshot.child("status").getValue().toString();
                    String refUserName = dataSnapshot.child("name").getValue().toString();
                    String refUserFullName = dataSnapshot.child("full_name").getValue().toString();
                    String refUserDOB = dataSnapshot.child("dob").getValue().toString();
                    String refUserGender = dataSnapshot.child("gender").getValue().toString();

                    Glide.with(SettingsActivity.this)
                            .load(refUserImage)
                            .override(200, 200)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(civ_user_image);

                    userStatus.setText(refUserStatus);
                    userName.setText(refUserName);
                    userFullName.setText(refUserFullName);
                    userdDOB.setText(refUserDOB);
                    userGender.setText(refUserGender);

                    progressDialog.dismiss();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriImage = result.getUri();
                Glide.with(SettingsActivity.this)
                        .load(uriImage)
                        .override(200, 200)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(civ_user_image);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void sendUserToDashboardActivity() {
        Intent dashboardIntent = new Intent(SettingsActivity.this, DashboardActivity.class);
        dashboardIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(dashboardIntent);
        finish();
    }
}
