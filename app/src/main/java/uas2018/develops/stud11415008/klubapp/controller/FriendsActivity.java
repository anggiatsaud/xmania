package uas2018.develops.stud11415008.klubapp.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.FriendsAdapter;
import uas2018.develops.stud11415008.klubapp.model.Friend;

public class FriendsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView myFriendList;

    private DatabaseReference friendRef;
    private FirebaseAuth auth;

    private String online_user_id;
    private List<Friend> friends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        auth = FirebaseAuth.getInstance();
        online_user_id = auth.getCurrentUser().getUid();
        friendRef = FirebaseDatabase.getInstance().getReference().child("Friends").child(online_user_id);

        toolbar = findViewById(R.id.toolbar_page_friends);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Friends");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myFriendList = findViewById(R.id.friend_list);
        myFriendList.setHasFixedSize(true);

        friends = new ArrayList<>();

    }

    @Override
    protected void onStart() {
        super.onStart();

        friendRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                friends.clear();

                if (dataSnapshot.exists()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        Friend friend = ds.getValue(Friend.class);
                        friends.add(friend);
                    }

                    myFriendList.setLayoutManager(new LinearLayoutManager(FriendsActivity.this));
                    FriendsAdapter friendsAdapter = new FriendsAdapter(FriendsActivity.this);
                    friendsAdapter.setFriends(friends);
                    myFriendList.setAdapter(friendsAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
