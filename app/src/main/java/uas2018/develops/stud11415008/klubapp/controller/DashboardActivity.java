package uas2018.develops.stud11415008.klubapp.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.ThreadAdapter;
import uas2018.develops.stud11415008.klubapp.model.Thread;

public class DashboardActivity extends AppCompatActivity {

    private static final String TAG = "DashboardActivity";
    private FirebaseAuth auth;
    private DatabaseReference dr_user;
    private DatabaseReference dr_thread;

    private Query queryThread;

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;

    private CircleImageView civ_user_image;
    private TextView tv_username;

    private String currentUserID;
    private List<Thread> threads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        auth = FirebaseAuth.getInstance();
        currentUserID = auth.getCurrentUser().getUid();
        dr_user = FirebaseDatabase.getInstance().getReference().child("Users");
        dr_thread = FirebaseDatabase.getInstance().getReference().child("Threads");

        queryThread = dr_thread.orderByChild("counter");

        toolbar = findViewById(R.id.main_page_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");

        drawerLayout = findViewById(R.id.drawable_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(DashboardActivity.this, drawerLayout, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView = findViewById(R.id.navigation_view);

        threads = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        recyclerView = findViewById(R.id.all_users_post_thread);

        View navView = navigationView.inflateHeaderView(R.layout.navigation_header);
        civ_user_image = navView.findViewById(R.id.nav_profile_image);
        tv_username = navView.findViewById(R.id.nav_user_full_name);

        dr_user.child(currentUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String fullname = dataSnapshot.child("name").getValue().toString();
                    String image = dataSnapshot.child("image_url").getValue().toString();

                    tv_username.setText("@"+fullname);
                    Glide.with(DashboardActivity.this)
                            .load(image)
                            .override(150, 150)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(civ_user_image);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                userMenuSelector(item);
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (id == R.id.icon_add){
            sendUserToCreateThread();
        }

        return super.onOptionsItemSelected(item);

    }

    private void userMenuSelector(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_add_post_thread:
                sendUserToCreateThread();
                break;

            case R.id.nav_profile:
                sendUserToProfileActivity();
                break;

            case R.id.nav_menu:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                break;

            case R.id.nav_forum:
                sendUserToForumActivity();
                break;

            case R.id.nav_friends:
                sendUserToFriendsActivity();
                break;

            case R.id.nav_find:
                sendUserToFindFriendActivity();
                break;

            case R.id.nav_messages:
                sendUserToMessageActivity();
                break;

            case R.id.nav_settings:
                sendUserToSettingsActivity();
                break;

            case R.id.nav_logout:
                auth.signOut();
                sendUserToLoginActivity();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        final FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser == null){
            sendUserToLoginActivity();
        }else{
            checkUserExistence();
        }

        progressDialog.setTitle("Show Thread");
        progressDialog.setMessage("Please wait, while we are showing all thread....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        queryThread.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                threads.clear();

                if (dataSnapshot.exists()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        Thread thread = ds.getValue(Thread.class);
                        threads.add(thread);
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(DashboardActivity.this));
                    ThreadAdapter threadAdapter = new ThreadAdapter(DashboardActivity.this);
                    threadAdapter.setThreads(threads);
                    recyclerView.setAdapter(threadAdapter);

                    Log.i(TAG, "Successfully Showing Thread");
                }

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });

    }

    private void checkUserExistence() {
        final String current_user_id = auth.getCurrentUser().getUid();

        dr_user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(current_user_id)){
                    sendUserToAccountActivity();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendUserToAccountActivity() {
        Intent accountIntent = new Intent(DashboardActivity.this, AccountActivity.class);
        accountIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(accountIntent);
    }

    private void sendUserToLoginActivity() {
        Intent loginIntent = new Intent(DashboardActivity.this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }

    private void sendUserToCreateThread() {
        Intent createThreadIntent = new Intent(DashboardActivity.this, CreateThreadActivity.class);
        startActivity(createThreadIntent);
        finish();
    }

    private void sendUserToSettingsActivity() {
        Intent settingsIntent = new Intent(DashboardActivity.this, SettingsActivity.class);
        startActivity(settingsIntent);
    }

    private void sendUserToForumActivity() {
        Intent settingsIntent = new Intent(DashboardActivity.this, ForumActivity.class);
        startActivity(settingsIntent);
    }

    private void sendUserToProfileActivity() {
        Intent settingsIntent = new Intent(DashboardActivity.this, ProfileActivity.class);
        startActivity(settingsIntent);
    }

    private void sendUserToFindFriendActivity() {
        Intent findIntent = new Intent(DashboardActivity.this, FindFriendsActivity.class);
        startActivity(findIntent);
    }

    private void sendUserToFriendsActivity() {
        Intent friendsIntent = new Intent(DashboardActivity.this, FriendsActivity.class);
        startActivity(friendsIntent);
    }

    private void sendUserToMessageActivity() {
        Intent friendsIntent = new Intent(DashboardActivity.this, UserMessageActivity.class);
        startActivity(friendsIntent);
    }

}
