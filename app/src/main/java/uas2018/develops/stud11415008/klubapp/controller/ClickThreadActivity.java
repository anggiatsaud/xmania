package uas2018.develops.stud11415008.klubapp.controller;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.tag.TagMessage;

public class ClickThreadActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView textViewDescription;
    private Button btnDelete, btnEdit;
    private ImageView imageViewThread;
    private ProgressDialog progressDialog;

    private String threadKey, currentUserId, databaseUserId;
    private TagMessage tagMessage;

    private DatabaseReference threadRef;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_thread);

        tagMessage = new TagMessage();
        threadKey = getIntent().getExtras().get(tagMessage.MESSAGE_KEY_THREAD).toString();

        toolbar = findViewById(R.id.toolbar_click_thread);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("View Thread");

        textViewDescription = findViewById(R.id.textView_desc_thread);
        btnEdit = findViewById(R.id.btn_edit_thread);
        btnDelete = findViewById(R.id.btn_delete_thread);
        imageViewThread = findViewById(R.id.imageView_thread);
        progressDialog = new ProgressDialog(this);

        btnEdit.setVisibility(View.INVISIBLE);
        btnDelete.setVisibility(View.INVISIBLE);

        auth = FirebaseAuth.getInstance();
        currentUserId = auth.getCurrentUser().getUid();
        threadRef = FirebaseDatabase.getInstance().getReference().child("Threads").child(threadKey);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCurrentThread();
            }
        });

    }

    private void deleteCurrentThread() {
        threadRef.removeValue();
        SendUserToDashboardActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            SendUserToDashboardActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    private void SendUserToDashboardActivity() {
        Intent dashboardIntent = new Intent(ClickThreadActivity.this, DashboardActivity.class);
        startActivity(dashboardIntent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        progressDialog.setTitle("Show Thread");
        progressDialog.setMessage("Please wait, while we are showing the thread....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        threadRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    final String desc = dataSnapshot.child("desc").getValue().toString();
                    String image = dataSnapshot.child("image_url").getValue().toString();
                    databaseUserId = dataSnapshot.child("user_key").getValue().toString();
                    textViewDescription.setText(desc);
                    Glide.with(ClickThreadActivity.this)
                            .load(image)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewThread);

                    if (currentUserId.equals(databaseUserId)){
                        btnEdit.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.VISIBLE);
                    }

                    btnEdit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editCurrentPost(desc);
                        }
                    });

                }

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }

    private void editCurrentPost(String desc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ClickThreadActivity.this);
        builder.setTitle("Edit Thread...");
        final EditText inputField = new EditText(ClickThreadActivity.this);
        inputField.setText(desc);
        builder.setView(inputField);

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                threadRef.child("desc").setValue(inputField.getText().toString());
                Toast.makeText(ClickThreadActivity.this, "Thread Updated successfully..", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        Dialog dialog = builder.create();
        dialog.show();

    }
}
