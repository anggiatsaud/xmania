package uas2018.develops.stud11415008.klubapp.controller;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.FindFriendsAdapter;
import uas2018.develops.stud11415008.klubapp.model.FindFriend;

public class FindFriendsActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private ImageButton searchFriendButton;
    private EditText searchInputText;

    private RecyclerView searchFriendResult;
    private List<FindFriend> findFriends;

    private DatabaseReference findUserRef;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        findUserRef = FirebaseDatabase.getInstance().getReference().child("Users");

        toolbar = findViewById(R.id.find_friends_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Find Friends");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        searchFriendButton = findViewById(R.id.search_people_btn);
        searchInputText = findViewById(R.id.search_box_input);
        progressDialog = new ProgressDialog(this);

        searchFriendResult = findViewById(R.id.search_result_list);
        searchFriendResult.setHasFixedSize(true);
        findFriends = new ArrayList<>();

        searchFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchInput = searchInputText.getText().toString();
                if (TextUtils.isEmpty(searchInput)){
                    Toast.makeText(FindFriendsActivity.this, "Please insert text....", Toast.LENGTH_SHORT).show();
                }else{
                    searchPeople(searchInput);
                }
            }
        });
    }

    private void searchPeople(String searchInput) {
        progressDialog.setTitle("Searching.....");
        progressDialog.setMessage("Please wait, while we are searching your friend....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        Query queryFindFriends = findUserRef.orderByChild("full_name").startAt(searchInput).endAt(searchInput + "\uf8ff");

        queryFindFriends.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                findFriends.clear();

                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    FindFriend findFriend = ds.getValue(FindFriend.class);
                    findFriends.add(findFriend);
                }

                searchFriendResult.setLayoutManager(new LinearLayoutManager(FindFriendsActivity.this));
                FindFriendsAdapter findFriendsAdapter = new FindFriendsAdapter(FindFriendsActivity.this);
                findFriendsAdapter.setFindFriends(findFriends);
                searchFriendResult.setAdapter(findFriendsAdapter);

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        progressDialog.setTitle("Showing Information");
        progressDialog.setMessage("Please wait, while we are showing all user....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        findUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                findFriends.clear();

                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    FindFriend findFriend = ds.getValue(FindFriend.class);
                    findFriends.add(findFriend);
                }

                searchFriendResult.setLayoutManager(new LinearLayoutManager(FindFriendsActivity.this));
                FindFriendsAdapter findFriendsAdapter = new FindFriendsAdapter(FindFriendsActivity.this);
                findFriendsAdapter.setFindFriends(findFriends);
                searchFriendResult.setAdapter(findFriendsAdapter);

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }
}
