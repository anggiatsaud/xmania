package uas2018.develops.stud11415008.klubapp.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.ThreadAdapter;
import uas2018.develops.stud11415008.klubapp.model.Thread;

public class MyPostsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView my_post_list;

    private FirebaseAuth auth;
    private DatabaseReference threadRef;
    private Query queryThread;

    private String currentUserId;

    private List<Thread> threads;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);

        auth = FirebaseAuth.getInstance();
        currentUserId = auth.getCurrentUser().getUid();
        threadRef = FirebaseDatabase.getInstance().getReference().child("Threads");
        queryThread = threadRef.orderByChild("user_key").equalTo(currentUserId);

        toolbar = findViewById(R.id.toolbar_my_posts_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Posts");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        threads = new ArrayList<>();
        my_post_list = findViewById(R.id.my_post_list_view);

    }

    @Override
    protected void onStart() {
        super.onStart();
        queryThread.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    threads.clear();

                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        Thread thread = ds.getValue(Thread.class);
                        threads.add(thread);
                    }

                    my_post_list.setLayoutManager(new LinearLayoutManager(MyPostsActivity.this));
                    ThreadAdapter threadAdapter = new ThreadAdapter(MyPostsActivity.this);
                    threadAdapter.setThreads(threads);
                    my_post_list.setAdapter(threadAdapter);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
