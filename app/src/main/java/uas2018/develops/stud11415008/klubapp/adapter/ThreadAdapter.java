package uas2018.develops.stud11415008.klubapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
//import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

//import de.hdodenhof.circleimageview.CircleImageView;
import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.controller.ClickThreadActivity;
import uas2018.develops.stud11415008.klubapp.controller.CommentActivity;
import uas2018.develops.stud11415008.klubapp.model.Thread;
import uas2018.develops.stud11415008.klubapp.tag.TagMessage;

public class ThreadAdapter extends RecyclerView.Adapter<ThreadAdapter.ViewHolder> {

    FirebaseAuth auth;
    DatabaseReference dr_user, dr_likes;
    Boolean likeChecker = false;
    String currentUserId, currentThreadKey, threadUserKey;

    private List<Thread> threads;
    public Context context;

    private TagMessage tagMessage;

    public ThreadAdapter(Context context) {
        this.context = context;
    }

    public void setThreads(List<Thread> threads) {
        this.threads = threads;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.thread_list_item, parent, false);
        tagMessage = new TagMessage();
        auth = FirebaseAuth.getInstance();
        currentUserId = auth.getCurrentUser().getUid();
        dr_user = FirebaseDatabase.getInstance().getReference("Users");
        dr_likes = FirebaseDatabase.getInstance().getReference("Likes");
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        currentThreadKey = threads.get(position).getKey_thread();
        threadUserKey = threads.get(position).getUser_key();

        holder.setLikesButtonStatus(currentThreadKey);
        holder.setCommentButtonStatus(threads.get(position).getKey_thread());

        holder.tv_subject.setText(threads.get(position).getDesc());
        holder.tv_date_time.setText(threads.get(position).getTimestamp());
        Glide.with(context)
                .load(threads.get(position).getImage_url())
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_image_thread);

        dr_user.child(threadUserKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String fullname = dataSnapshot.child("name").getValue().toString();
                    String image = dataSnapshot.child("image_url").getValue().toString();

                    holder.tv_name.setText("@"+(fullname));
                    Glide.with(context)
                            .load(image)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.civ_image_user);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailIntent = new Intent(context, ClickThreadActivity.class);
                detailIntent.putExtra(tagMessage.MESSAGE_KEY_THREAD, threads.get(position).getKey_thread());
                context.startActivity(detailIntent);
            }
        });

        holder.iv_comment_thread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent commentIntent = new Intent(context, CommentActivity.class);
                commentIntent.putExtra(tagMessage.MESSAGE_KEY_THREAD, threads.get(position).getKey_thread());
                context.startActivity(commentIntent);
            }
        });

        holder.tv_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent commentIntent = new Intent(context, CommentActivity.class);
                commentIntent.putExtra(tagMessage.MESSAGE_KEY_THREAD, threads.get(position).getKey_thread());
                context.startActivity(commentIntent);
            }
        });

        holder.iv_like_thread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likeChecker = true;
                dr_likes.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (likeChecker.equals(true)){
                            if (dataSnapshot.child(threads.get(position).getKey_thread()).hasChild(currentUserId)){
                                dr_likes.child(threads.get(position).getKey_thread()).child(currentUserId).removeValue();
                                likeChecker = false;
                            }else{
                                dr_likes.child(threads.get(position).getKey_thread()).child(currentUserId).setValue(true);
                                likeChecker = false;
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return threads.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_subject;
        private TextView tv_name;
        private TextView tv_date_time;
        private TextView tv_likes;
        private TextView tv_comments;

        private CircleImageView civ_image_user;
        private ImageView iv_image_thread;
        private ImageView iv_like_thread;
        private ImageView iv_comment_thread;

        private View view;

        int countLikes, countComments;
        String currentUserId;
        DatabaseReference likesRef, dr_thread;

        ViewHolder(View itemView) {
            super(itemView);

            view = itemView;

            tv_subject = itemView.findViewById(R.id.view_thread_subject);
            tv_name = itemView.findViewById(R.id.thread_user_name);
            tv_date_time = itemView.findViewById(R.id.thread_date);
            tv_likes = itemView.findViewById(R.id.thread_like_count);
            tv_comments = itemView.findViewById(R.id.thread_comment_count);

            iv_image_thread = itemView.findViewById(R.id.thread_image);
            iv_like_thread = itemView.findViewById(R.id.thread_like_btn);
            iv_comment_thread = itemView.findViewById(R.id.blog_comment_icon);
            civ_image_user  = itemView.findViewById(R.id.thread_user_image);

            likesRef = FirebaseDatabase.getInstance().getReference().child("Likes");
            currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            dr_thread = FirebaseDatabase.getInstance().getReference().child("Threads");
        }

        void setLikesButtonStatus(final String currentThreadKey){
            likesRef.addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child(currentThreadKey).hasChild(currentUserId)){
                        countLikes = (int) dataSnapshot.child(currentThreadKey).getChildrenCount();
                        iv_like_thread.setImageResource(R.mipmap.like);
                        tv_likes.setText(Integer.toString(countLikes) + " Likes");
                    }else{
                        countLikes = (int) dataSnapshot.child(currentThreadKey).getChildrenCount();
                        iv_like_thread.setImageResource(R.mipmap.dislike);
                        tv_likes.setText(Integer.toString(countLikes) + " Likes");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        void setCommentButtonStatus(String threadKey) {
            dr_thread.child(threadKey).child("Comments").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){
                        countComments = (int) dataSnapshot.getChildrenCount();
                        tv_comments.setText(Integer.toString(countComments) + (" Comments"));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
