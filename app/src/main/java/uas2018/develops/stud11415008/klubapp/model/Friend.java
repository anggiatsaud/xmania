package uas2018.develops.stud11415008.klubapp.model;

public class Friend {
    public String date, key_user;

    public Friend() {
    }

    public Friend(String date, String key_user) {
        this.date = date;
        this.key_user = key_user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKey_user() {
        return key_user;
    }

    public void setKey_user(String key_user) {
        this.key_user = key_user;
    }
}
