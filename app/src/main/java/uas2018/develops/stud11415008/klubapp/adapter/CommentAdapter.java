package uas2018.develops.stud11415008.klubapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.model.Comment;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    DatabaseReference dr_user;

    String commentUserId;

    private List<Comment> comments;

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    private Context context;

    public CommentAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_item, parent, false);
        dr_user = FirebaseDatabase.getInstance().getReference("Users");
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tv_comment.setText(comments.get(position).getComment());
        holder.tv_date.setText(comments.get(position).getDateTime());
        holder.tv_time.setText(comments.get(position).getTimestamp());

        commentUserId = comments.get(position).getUser_key();

        dr_user.child(commentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String filename = dataSnapshot.child("name").getValue().toString();
                    String image = dataSnapshot.child("image_url").getValue().toString();

                    holder.tv_name.setText("@"+(filename));
                    Glide.with(context)
                            .load(image)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.civ_user);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView civ_user;
        private TextView tv_name;
        private TextView tv_comment;
        private TextView tv_date;
        private TextView tv_time;

        public ViewHolder(View itemView) {
            super(itemView);

            civ_user = itemView.findViewById(R.id.img_item_photo);
            tv_name = itemView.findViewById(R.id.comment_username);
            tv_comment = itemView.findViewById(R.id.comment);
            tv_date = itemView.findViewById(R.id.comment_date);
            tv_time = itemView.findViewById(R.id.comment_time);

        }
    }
}
