package uas2018.develops.stud11415008.klubapp.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.controller.ChatActivity;
import uas2018.develops.stud11415008.klubapp.controller.PersonProfileActivity;
import uas2018.develops.stud11415008.klubapp.model.Friend;
import uas2018.develops.stud11415008.klubapp.tag.TagMessage;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private Context context;

    private List<Friend> friends;

    public FriendsAdapter(Context context) {
        this.context = context;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    private DatabaseReference userRef;
    private Query queryUser;

    private TagMessage tagMessage;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        userRef = FirebaseDatabase.getInstance().getReference().child("Users");
        tagMessage = new TagMessage();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final String currentUserId = friends.get(position).getKey_user();

        holder.tv_date.setText(friends.get(position).getDate());



        userRef.child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    final String filename = dataSnapshot.child("full_name").getValue().toString();
                    String image = dataSnapshot.child("image_url").getValue().toString();

                    holder.tv_name.setText(filename);
                    Glide.with(context)
                            .load(image)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.civ_image);

                    holder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            CharSequence option[] = new CharSequence[]{
                                    filename + "'s Profile",
                                    "Send Message"
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Select Option");

                            builder.setItems(option, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0){
                                        Intent profileIntent = new Intent(context, PersonProfileActivity.class);
                                        profileIntent.putExtra(tagMessage.MESSAGE_KEY_USER, currentUserId);
                                        context.startActivity(profileIntent);
                                    }
                                    if (which == 1){
                                        Intent chatIntent = new Intent(context, ChatActivity.class);
                                        chatIntent.putExtra(tagMessage.MESSAGE_KEY_USER, currentUserId);
                                        context.startActivity(chatIntent);
                                    }
                                }
                            });

                            builder.show();
                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_date;
        private CircleImageView civ_image;
        private View mView;

        public ViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            tv_name = itemView.findViewById(R.id.user_profile_full_name);
            tv_date = itemView.findViewById(R.id.user_profile_status);
            civ_image = itemView.findViewById(R.id.user_profile_image);

        }
    }
}
