package uas2018.develops.stud11415008.klubapp.controller;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import uas2018.develops.stud11415008.klubapp.R;
import uas2018.develops.stud11415008.klubapp.adapter.CommentAdapter;
import uas2018.develops.stud11415008.klubapp.model.Comment;
import uas2018.develops.stud11415008.klubapp.tag.TagMessage;

public class CommentActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView commentList;
    private ImageButton commentBtn;
    private EditText commentInput;
    private ProgressDialog progressDialog;

    private List<Comment> comments;

    private String threadKey, userCurrentId, textComment;

    private TagMessage tagMessage;

    private FirebaseAuth auth;
    private DatabaseReference commentRef;
    private Query queryCom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        tagMessage = new TagMessage();
        threadKey = getIntent().getExtras().get(tagMessage.MESSAGE_KEY_THREAD).toString();

        toolbar = findViewById(R.id.toolbar_page_comment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Comment...");

        auth = FirebaseAuth.getInstance();
        userCurrentId = auth.getCurrentUser().getUid();
        commentRef = FirebaseDatabase.getInstance().getReference().child("Threads").child(threadKey).child("Comments");

        queryCom = commentRef.orderByChild("dateTime");

        comments = new ArrayList<>();

        commentList = findViewById(R.id.rv_comment);
        commentList.setHasFixedSize(true);
        progressDialog = new ProgressDialog(this);
        commentBtn = findViewById(R.id.comment_btn);
        commentInput = findViewById(R.id.comment_input);


        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validationComment();
            }
        });

    }

    private void validationComment() {
        textComment = commentInput.getText().toString();
        if (TextUtils.isEmpty(textComment)){
            Toast.makeText(this, "Please insert your comment", Toast.LENGTH_SHORT).show();
        }else{
            saveCommentToDatabase();
        }
    }

    private void saveCommentToDatabase() {
        Calendar calendarDate = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat currentDate = new SimpleDateFormat("dd MMMM yyyy");
        String dateTime = currentDate.format(calendarDate.getTime());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        String timeStamp = currentTime.format(calendarDate.getTime());

        String randomName = UUID.randomUUID().toString();
        String randomKey = "Comment-" + userCurrentId + randomName;

        HashMap commentsMap = new HashMap();
        commentsMap.put("comment_key", randomKey);
        commentsMap.put("user_key", userCurrentId);
        commentsMap.put("comment", textComment);
        commentsMap.put("dateTime", dateTime);
        commentsMap.put("timestamp", timeStamp);

        commentRef.child(randomKey).updateChildren(commentsMap)
                .addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()){
                            Toast.makeText(CommentActivity.this, "you have cemented successfully...", Toast.LENGTH_SHORT).show();
                            commentInput.setText("");
                        }else{
                            Toast.makeText(CommentActivity.this, "Error Occurred, try again...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            SendUserToDashboardActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    private void SendUserToDashboardActivity() {
        Intent dashboardIntent = new Intent(CommentActivity.this, DashboardActivity.class);
        startActivity(dashboardIntent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        progressDialog.setTitle("Show Comment");
        progressDialog.setMessage("Please wait, while we are showing all Comment....");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(true);

        queryCom.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                comments.clear();

                if (dataSnapshot.exists()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        Comment comment = ds.getValue(Comment.class);
                        comments.add(comment);
                    }

                    commentList.setLayoutManager(new LinearLayoutManager(CommentActivity.this));
                    CommentAdapter commentAdapter = new CommentAdapter(CommentActivity.this);
                    commentAdapter.setComments(comments);
                    commentList.setAdapter(commentAdapter);
                }

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }
}
