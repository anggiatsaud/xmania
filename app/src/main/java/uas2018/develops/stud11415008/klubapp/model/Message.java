package uas2018.develops.stud11415008.klubapp.model;

public class Message {
    private String dateTime, time, type, message, from;

    public Message() {
    }

    public Message(String dateTime, String time, String type, String message, String from) {
        this.dateTime = dateTime;
        this.time = time;
        this.type = type;
        this.message = message;
        this.from = from;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
